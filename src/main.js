import Vue from 'vue';
import VueOnsen from 'vue-onsenui';

import 'onsenui';
// Onsen UI Styling and Icons
require('onsenui/css-components-src/src/onsen-css-components.css');
require('onsenui/css/onsenui.css');
require('Styles/style.css');


// ES6 Modules or TypeScript
import swal from 'sweetalert2'

// CommonJS

import store from './store/store';
import App from './App';
import axios from 'axios';
import mix from './mixins/test';
import Toasted from 'vue-toasted';
import moment from 'moment'


Vue.use(require('vue-moment'));

Vue.use(Toasted);
Vue.prototype.moment = moment


Vue.mixin(mix);

Vue.prototype.base_url = 'http://18.188.115.65';
//Vue.prototype.base_url = 'http://172.104.166.132';
Vue.prototype.exam_api = '/api/v1/exam/exams/';
Vue.prototype.exam_result_api = '/api/v1/exam/response/user/';
Vue.prototype.score_api = '/api/v1/exam/performance/user/';
Vue.prototype.signup_api = '/api/v1/connect-user/connect-users/';
Vue.prototype.signin_mobile_api = '/api/v1/connect-user/connect-user/login/mobile/';
Vue.prototype.forgot_password_api = '/api/v1/connect-user/connect-user/forgot-password/';
Vue.prototype.update_profile_api = '/api/v1/connect-user/connect-user/mobile/';
Vue.prototype.article_api = '/api/v1/article/articles/';
Vue.prototype.performance_api = '/api/v1/exam/performance/user/';
Vue.prototype.exam_history = '/api/v1/exam/response/user/';
Vue.prototype.subject_details = '/api/v1/exam/exam/';
Vue.prototype.$http = axios;

Vue.use(VueOnsen);


// CommonJS


module.exports =

    new Vue({
        el: '#app',
        store,
        template: '<app></app>',
        components: {App},

        created() {
            navigator.splashscreen.hide();
        },
        mounted() {
            //localStorage.clear();
        },

        computed: {}
    });

