import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    user: {
        id: "",
        name: "",
        email: "",
        mobile: "",
        gender: "",
        bday: "",
        fb_id: "",

    },
    exam: {
        exam_id: "",
        questions: [],
        started_at: "",
        ended_at: ""
    },
    time: {
        duration: ""
    },
    exam_details: {
        details: [],
    },
    wp_details: {

        articles_id: []

    },
    subject_performance: {
        subject_lists: []
    },
    individual_subject: {
        subject_id_click: ''
    },
    article_fetch: {
        articleId: '',
        article_link: ''

    },
    user_performance_days: {
        day: ''
    }
}


const mutations = {
    setUser(state, data) {
        state.user.id = typeof data.id !== 'undefined' ? data.id : state.user.id;
        state.user.name = typeof data.name !== 'undefined' ? data.name : state.user.name;
        state.user.email = typeof data.email !== 'undefined' ? data.email : state.user.email;
        state.user.mobile = typeof data.mobile !== 'undefined' ? data.mobile : state.user.mobile;
        state.user.gender = typeof data.gender !== 'undefined' ? data.gender : state.user.gender;
        state.user.bday = typeof data.bday !== 'undefined' ? data.bday : state.user.bday;
        state.user.fb_id = typeof data.fb_id !== 'undefined' ? data.fb_id : state.user.fb_id;


    },
    questionInfo(state, data) {
        state.exam.exam_id = typeof data.exam_id !== 'undefined' ? data.exam_id : state.exam.exam_id;
        state.exam.questions = typeof data.questions !== 'undefined' ? data.questions : state.exam.questions;

    },
    durations(state, data) {
        state.time.duration = data.duration;
    },
    examDetails(state, data) {
        state.exam_details.details = data.details;
    },
    wordpressDetails(state, data) {
        state.wp_details.articles_id = data.articles_id;
    },
    getPerformance(state, data) {
        state.subject_performance.subject_lists = data.subject_lists;
    },
    getSubject(state, data) {
        state.individual_subject.subject_id_click = data.subject_id_click;
    },
    getArticle(state, data) {
        state.article_fetch.articleId = data.articleId;
        state.article_fetch.article_link = data.article_link;

    },
    getDays(state, data) {
        state.user_performance_days.day = data.day;
    }


}

export default new Vuex.Store({
    state,
    mutations
});