//  Global Mixins to import test questions from server

import test from "../test";
import dashboard from "../dashboard";
import login from "../LogIn";
import moment from "moment";
import swal from "sweetalert2";

module.exports = {
    methods: {
        question: function (timer) {
            var time = {
                duration: timer
            };
            this.$store.commit("durations", time);
            console.log(this.$store.state.time.duration);

            var that = this;
            that
                .$http({
                    method: "post",
                    url: this.base_url + this.exam_api,
                    auth: {
                        username: "l360_mobile_app",
                        password: "itsd321#"
                    },
                    data: {
                        id: this.$store.state.user.id,
                        duration: this.$store.state.time.duration
                    }
                })
                .then(response => {

                    if (response.status === 200) {
                        var exam = {
                            exam_id: response.data.exam_id,
                            questions: response.data.questions
                        };

                        that.$store.commit("questionInfo", exam);
                        this.questions = this.$store.state.exam.questions;

                        swal({
                            title: "Before you start",
                            type: "info",

                            html:
                                "<ol>" +
                                "<li><b>This exam is " +
                                this.$store.state.time.duration +
                                " minutes long." +
                                "</b></li>" +
                                "<li><b>You can leave the exam anytime.</b></li>" +
                                "<li><b>The exam you quit won't be considered. </b> </li> " +
                                "</ol>" +
                                " <p><b> Press OK to start the exam  </b></p>",
                            confirmButtonColor: "#3085d6",
                            confirmButtonText: "OK"
                        }).then(result => {
                            if (result.value) {
                                this.pageStack.push(test);
                            }
                        });
                    }
                })
                .catch(error => {
                    swal({
                        title: "Oops!",
                        text: error.response,
                        type: "error"
                    });
                });
        },
        check: function () {
            const val = JSON.parse(localStorage.getItem("keys"));
            if (val) {
                const mobile = val.mobile;
                const pass = val.pass;

                this.$http({
                    method: "post",
                    url: this.base_url + this.signin_mobile_api + mobile + "/",
                    auth: {
                        username: "l360_mobile_app",
                        password: "itsd321#"
                    },
                    data: {
                        password: pass
                    }
                })
                    .then(res => {

                        if (res.status == 200) {
                            var user = {
                                id: res.data.id,
                                name: res.data.name,
                                birthday: res.data.birthday,
                                email: res.data.email,
                                fb_id: res.data.fb_id,
                                gender: res.data.gender,
                                mobile: res.data.mobile
                            };

                            this.$store.commit("setUser", user);

                            this.pageStack.splice(0, 1);
                            this.pageStack.push(dashboard);
                        }
                    })
                    .catch(err => {

                        if (err.response === undefined) {
                            swal({
                                title: "Oops!",
                                text: "Network problem",
                                type: "error"
                            });
                        } else {
                            swal({
                                title: "Oops!",
                                text: err.response,
                                type: "error"
                            });

                            this.pageStack.push(dashboard);
                        }
                    });
            } else {

                this.pageStack.push(login);

            }
        },
        home_to: function () {

            this.pageStack.push(dashboard);
        }
        /*
                back: function () {
                    this.$ons.ready(() => {
                        this.$ons.disableDeviceBackButtonHandler();
                        var myNavigator = document.getElementById("Navigator");

                        var page = myNavigator.topPage.name;
                        console.log(page);


                        document.addEventListener(
                            "backbutton",
                            e => {
                                if (page !== "test") {
                                    this.$ons.notification
                                        .confirm("Do you want to close the app?") // Ask for confirmation
                                        .then(function (index) {
                                            if (index === 1) {
                                                // OK button
                                                navigator.app.exitApp(); // Close the app
                                            }
                                        });
                                }

                                e.preventDefault();
                                e.stopPropagation();


                            },
                            true
                        );

                        //alert("hello");
                    });
                }*/
    }
};
