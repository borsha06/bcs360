//  Rendering Horizontal Bar Chart

import { Line } from "vue-chartjs";

export default {
  name: "diagram",
  extends: Line,
  data: () => ({
    show: false,
    chartData: "",
    score: {},
    values: [],
    keys: [],
    subject_name: "",

    options: {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: "Your Performance Over Time"
      },
        scales: {
            xAxes: [{
                ticks: {
                    display: false
                }
            }]
        }
    }
  }),

  created() {
    // Overwriting base render method with actual data.
    this.$http({
      method: "get",
      url:
        this.base_url +
        this.performance_api +
        this.$store.state.user.id +
        "/subject/" +
        this.$store.state.individual_subject.subject_id_click +
        "/",
      auth: {
        username: "l360_mobile_app",
        password: "itsd321#"
      }
    }).then(res => {
      console.log(res);
      if (res.status == "200") {
        for (var i = 0; i < res.data.performance_data.length; i++) {
          this.values.push(res.data.performance_data[i].x);
          this.keys.push(res.data.performance_data[i].y);
        }
        // console.log(this.keys);
        // console.log(this.values);
        this.subject_name = res.data.subject;

        if (res.data.performance_data.length === 0) {
          var no_data = "No Data";

          this.subject_name = no_data;
          this.values.push(no_data);
          this.keys.push(no_data);
        }

        this.renderChart(
          (this.chartData = {
            labels: this.keys,

            datasets: [
              {
                label: this.subject_name,
                backgroundColor: "#2196F3",
                pointRadius: 4,
                pointBackgroundColor: "white",
                pointBorderColor: "#1b2e63",
                data: this.values
              }
            ]
          }),
          this.options
        );
      }
    });
  }
};
/*
import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: Line,
    mixins: [reactiveProp],
    props: ['options'],
    mounted () {
        // this.chartData is created in the mixin.
        // If you want to pass options please create a local options object
        this.renderChart(this.chartData, this.options)
    }
}
*/
